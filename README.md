# Vodafone Iphone12 Checkout Test cases 
Hello and welcome to the automation testing project for Vodafone.
In this document you will find all the information about this project.

The goal of this task was to deliver:

* To build a test cases for the checkout functionality of iPhone20 red color.
* A report with the results of the testing.
* Automate the test using cypress.
* Write down the challenges.

so lets start

### Test strategy.

I have desided to divide the automated test into two parts: 
* ui testing.
* api testing.

lets start with the test cases :) 

I will cover in this assignment the below test cases:

### Test cases
| TC_ID  | Test type | Test Description                                                | Test data                                                           | Steps                                                                                                                                                                                    | Expected results                                                                                                                                                                                                                                                                                    |
|--------|-----------|-----------------------------------------------------------------|---------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| TC_001 | UI        | should display the correct title                                | abonnement/mobiel/telefoon/apple-iphone-12-128gb-black              | 1- Perform the get request. 2- Get the device name from the response "device_group_name" 3- Check the title that is appearing in the UI                     | The title should be the same value of the "device_group_name" property from the response body                                                                                                                                                                                                       |
| TC_002 | UI        | should display all the available capacity options               | abonnement/mobiel/telefoon/apple-iphone-12-128gb-black              | 1- Perform the get request. 2- Get all the available capacity from the response "data_capacity_variants" 3- Check the number of options available in the UI | The length of the "data_capacity_variants" array should equal the number of options for the capacity                                                                                                                                                                                                |
| TC_003 | UI        | should display all the available colors depends on the capacity | abonnement/mobiel/telefoon/apple-iphone-12-128gb-black              | 1- Perform the get request. 2- Get all the available capacity from the response "data_capacity_variants" 3- Get all the available colors for each capacity. | The number of colors in the response per capacity should match in the UI                                                                                                                                                                                                                            |
| TC_004 | UI        | should display the correct default color on the based capacity  | abonnement/mobiel/telefoon/apple-iphone-12-128gb-black              | 1- Perform the get request. 2- Get all the available capacity from the response "data_capacity_variants" 3- Get the default color for each capacity.        | 1- Every capacity option should show the correct default color 2- The default color should be part of the URL but in english language.                                                                                                                                                              |
| TC_005 | UI        | should be able to select all the colors for all the capacities  |                                                                     | 1- Click on every available capacity option. 2- Click on every color available                                                                                                           | 1- The correct color should appear in the url 2- The selected color should be shown as active (red circle around it)                                                                                                                                                                                |
| TC_006 | UI        | should be able to select "i have ziggo account"                 |                                                                     | 1- Open the phone page 2- Click on i have Ziggo account                                                                                                                                  | 1- The URL should contain ziggo=true 2- I am a Ziggo customer option should be marked as selected                                                                                                                                                                                                   |
| TC_007 | UI        | should be able to select "i dont have ziggo account"            |                                                                     | 1- Open the phone page 2- Click on I dont have Ziggo account                                                                                                                             | 1- The URL should contain ziggo=false 2- I am not a Ziggo customer option should be marked as selected                                                                                                                                                                                              |
| TC_008 | UI        | should be able to choose any package                            |                                                                     | 1- Open the website. 2- Click on every single package. 3- Check the url and the overview                                                                                                 | 1- The overview should contain the same price that appears in the package if the customer is not a Ziggo subscriber 2- The overview should contain the same price that appears in the package minus the discount if the customer is a Ziggo subscriber 3- The URL should contains the package name  |
| TC_009 | API       | should get all the data from the API                            | /rest/catalogues/consumer/pdp/devices/device-groups/apple-iphone-12 | 1- Perform a get request for the path in the test data                                                                                                                                   | 1- The status code should be 200 2- The brand value should be correct. 3- The device_group_name value should be correct. 4- The id value should be correct. 5- The name value should be correct.                                                                                                    |


### Automation testing:

make it work and then make it beautiful ...

I used cypress to automated all the test cases above, i did not use any automation framework design techniques like page object and cucumber due to time limitations and i did not use cypress custom functions.
So the code is not really a maintinance friendly, but in the real life it will be different.
I did not cover the cross browser testing, or run the test in parallel in this task and i did not cover all the scenarios in checkout page :) 

### challenges: 
* I did not have any knowladge about the APIs and the logic, so i had to figure them out by my self
* Some of the colors in the response are written in dutch and some of it in english, check out the below snapshot

![Alt text](images/response.png)
* Sometime and for some devices it will not show all the availbe colors that are returned from the API, so maybe there is more logic that i did not figure out in there :) 

![Alt text](images/colors.png)


### Run the test locally:
* clone the project and make sure that you are in vodafone-assessment directory `cd vodafone-assessment`
* install all the dependencies for cypress `npm install`
* Run all the tests `npm run all:test`
* Run all the tests using docker `npm run all:docker`.


### Test reporting:
* I will use cypress dashboard for reporting.
* When we run the test using shell headless mode or in docker or in gitlab, all the results will be in cypress dashboard.
* You can check all the results on this link https://dashboard.cypress.io/projects/r1rbo7/runs/.
* Logs, videos and screen shot are available there.
* More insights can be done, like the slowest tests, flaky tests and ets.
* This service is free if the number of test cases is less that 500 monthly.

 
