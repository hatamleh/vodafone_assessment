// export const phone = "apple-iphone-12";
// export const url = "abonnement/mobiel/telefoon/apple-iphone-12-128gb-black"

export const phones = [
    {
        phone: "apple-iphone-12",
        url: "abonnement/mobiel/telefoon/apple-iphone-12-128gb-black"
    }
]

export const colorsInEnglish = (colors) => {
    let englishColors = []
    colors.forEach(color => {
        switch (color) {
            case "Zwart":
                englishColors.push("black")
                break;
            case "Wit":
                englishColors.push("white")
                break
            case "Rood":
                englishColors.push("red")
                break
            case "Blauw":
                englishColors.push("blue")
                break
            case "green":
                englishColors.push("green")
                break
            case "Groen":
                englishColors.push("green")
                break
        }
    })

    return englishColors.sort()
}

export const colorInEnglish = (color) => {
    let colorInEnglish;
    switch (color) {
        case "Zwart":
            colorInEnglish = "black"
            break;
        case "Wit":
            colorInEnglish = "white"
            break
        case "Rood":
            colorInEnglish = "red"
            break
        case "Blauw":
            colorInEnglish = "blue"
            break
        case "green":
            colorInEnglish = "green"
            break
        case "Groen":
            colorInEnglish = "green"
            break
    }

    return colorInEnglish
}