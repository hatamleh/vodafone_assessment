
import * as config from '../../support/config'

config.phones.forEach(phone => {
    describe(`Test all the UI functionality for checkout feature for ${phone}`, () => {
        let mobileData;

        before('collect all the data from the API', () => {
            cy.request({
                method: 'GET',
                url: `/rest/catalogues/consumer/pdp/devices/device-groups/${phone.phone}`
            }).then(response => {
                mobileData = response.body.data
            }).then(() => {
                cy.setCookie("WRIgnore", "true")
                cy.visit(`https://www.vodafone.nl/${phone.url}`)
                cy.get('.cookiewall__accept').click({ force: true })
                cy.get('[data-testid=vfz-button-converged-status-prompt--yes]').click({ force: true })
            })

        })


        it('should display the correct title', () => {

            let nameFromAPI = mobileData[0].device_group_name;
            cy.get('h1.vfz-mobile-pdp__page-title').invoke('text').should('eql', nameFromAPI)
        })

        it('should display all the available capacity options', () => {
            let capacitys = mobileData[0].data_capacity_variants
            cy.get('.capacity__option').should('have.length', capacitys.length)
            capacitys.forEach((capacity, i) => {
                let capacityFromAPI = capacitys[i].storage
                cy.get('.capacity__option').eq(i).should('contain.text', capacityFromAPI)
            })

        })

        it('should display all the available colors depends on the capacity', () => {
            let capacitys = mobileData[0].data_capacity_variants
            capacitys.forEach((cap, i) => {
                let colorsFromAPI = capacitys[i].colors
                cy.get(`[data-testid="vfz-color-selector--${i}"]`).click({ force: true })
                cy.get('.color-selector__ball').should('have.length', colorsFromAPI.length)
            })
        })

        it('should display the correct default color on the based capacity', () => {
            let capacitys = mobileData[0].data_capacity_variants
            capacitys.forEach((cap, i) => {
                let defaultColorsFromAPI = capacitys[i].default_color
                let englishColors = config.colorInEnglish(defaultColorsFromAPI)
                cy.get('.capacity__option input').eq(i).click({ force: true })
                cy.url().should('contains', englishColors[0])
            })
        })

        it('should be able to select all the colors for all the capacities', () => {
            let capacitys = mobileData[0].data_capacity_variants
            capacitys.forEach((capacity, i) => {
                cy.get('.capacity__option input').eq(i).click({ force: true })
                let colors = capacity.colors
                let englishColors = config.colorsInEnglish(colors)
                colors.forEach((color, i) => {
                    cy.log(englishColors)
                    cy.get(`[data-testid=vfz-color-selector--${i}]`).click({ force: true })
                    cy.get(`[data-testid=vfz-color-selector--${i}]`).should('have.class', 'color-selector__ball--active')
                    cy.url().should('contain', englishColors[i])
                })
            })
        })

        it('should be able to select "i have ziggo account"', () => {
            cy.get('input[name="customer-is-ziggo"]').eq(0).click({ force: true })
            cy.url().should('contain', "ziggo=true")
            cy.get('input[name="customer-is-ziggo"]').eq(0).should('have.value', "true")
            cy.get('input[name="customer-is-ziggo"]').eq(0).next().should('have.class', 'vfz-button-style-radio-button__button--selected')
        })

        it('should be able to select "i dont have ziggo account"', () => {
            cy.get('input[name="customer-is-ziggo"]').eq(1).click({ force: true })
            cy.url().should('contain', "ziggo=false")
            cy.get('input[name="customer-is-ziggo"]').eq(1).should('have.value', "false")
            cy.get('input[name="customer-is-ziggo"]').eq(1).next().should('have.class', 'vfz-button-style-radio-button__button--selected')
        })

        it('should be able to choose any package', () => {
            cy.get('input[name="customer-is-ziggo"]').eq(1).click({ force: true })
            cy.get('.vfz-subscription-listing__gfx-container').then($el => {
                for (let index = 0; index < $el.length; index++) {
                    cy.get('.vfz-subscription-listing__gfx-container').eq(index).click({ force: true })
                    cy.wait(500)
                    cy.get('.vfz-subscription-listing__converged-price').eq(index).invoke('text').then(packagePrice => {
                        cy.get('[data-testid="vfz-pdp-receipt--item-price"]').invoke('text').then(overviewPrice => {
                            expect(packagePrice.replace(/^\D+/g, '')).to.contains(overviewPrice.replace(/^\D+/g, ''))
                        })

                    })
                    cy.get('.vfz-subscription-listing__subscription-name').eq(index).invoke('text').then(text => {
                        cy.url().should('contain', text.toLowerCase().replace(" ", "-"))
                    })

                }
            })
        })

    })
})


