
describe('Check all the checkout finctionality for iPhone', () => {

    it('should get all the data from the API', () => {
        cy.request({
            method: 'GET',
            url: '/rest/catalogues/consumer/pdp/devices/device-groups/apple-iphone-12'
        }).then(response => {
            expect(response.status).to.eq(200)
            expect(response.body.data[0].brand).to.eq('Apple')
            expect(response.body.data[0].device_group_id).to.eq('apple-iphone-12')
            expect(response.body.data[0].device_group_name).to.eq('iPhone 12')
            expect(response.body.data[0].id).to.eq('apple-iphone-12-64gb')
            expect(response.body.data[0].name).to.eq('Apple iPhone 12 64GB')
        })
    })
})